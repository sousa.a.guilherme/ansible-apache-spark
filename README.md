# Apache Spark Deployment

## Question 1:

Automate the deploy process for an Apache Spark cluster with 1 master node and 2 slave nodes.

Apache Spark: https://spark.apache.org

You are free to use the tools that you are most familiar with. 

Please submit the code into a repository that can be shared with us for review.

### Proposed Solution:

In this repository you can find an Ansible role that is capable of deploying an Apache Spark cluster with 1 master node and an arbitrary number of worker nodes (funny note:  <a href="https://www.theregister.co.uk/2018/09/11/python_purges_master_and_slave_in_political_pogrom/">Python joins movement to dump 'offensive' master, slave terms</a>).

I've tested the solution with some local Virtualbox VM's and the following use cases:

- 1 Master and 2 Workers
- 1 Master and 2 Workers and adding a new Worker after the first deployment (no downtime)
- 1 Master and 4 Workers

In any case, the cluster should not restart if a new node is added.

For a matter of simplicity, there's a folder with Vagrantfiles to facilitate the tests.

(Note: edit the Vagrantfile to point to your ssh pubkey; currently it's set to `/root/.ssh/id_rsa.pub`)

#### Requirements:

- Ansible (tested with 2.5.1; any recent version should work)
- Vagrant (tested with 2.2.1)
- Virtualbox (tested with 5.2.14)
- Centos 7.5 Minimal (if not using vagrant for provisioning)


#### Procedure:

##### Create a 1 Master and 2 Worker Cluster:

	$ cd vagrant/1master2slave/
	$ vagrant up
	$ cd ../..
	$ ansible-playbook -i inventory -l all apache-spark.yml
	
##### Add a new Worker:
	
	$ cd vagrant/add1slave/
	$ vagrant up
	$ cd ../..
	$ vim inventory (uncoment a worker node)
	$ ansible-playbook -i inventory -l all apache-spark.yml

The role was writen in order to be idempontent; it can be executed at any time without impact, and will correct any configuraiton deviation that may have occurred.

##### Ansible play:


![](images/ansible-apache-spark.gif)


##### Spark Master Web UI:

![](images/spark-master.jpg)



## Question 2:

What would be your strategy to deploy and maintain an Apache Spark cluster on Azure.

Exploratory question for a strategy, tools to be used for deployment, maintenance, monitoring and alerts. 

Please explain briefly your options.

### Proposed Solution:

First of all, I'd like to highlight that the proposed solution for Question 1 does not provide a high availability deployment as such is not possible with only one master.
In order to achieve this, either the deployment has to be done with Hadoop or Mesos (with Zookeeper for Master member election).
For what I have found, Spark achieves better performance with Mesos than with Hadoop, so I would go with the first option if Hadoop is not required for any other tasks.

Regarding maintenance and upgrades, I would strongly consider rotating the entire cluster by deploying a new one each time a new version is needed.
Rolling updates/upgrades are not an option, as Spark does not support running with different versions between nodes (updating just the Operating System shouldn't be a problem as there are few dependencies); in this case, an in place upgrade would have downtime since the cluster would have to be stopped for the operation.

Deploying a new cluster with a newer version could be achieved with Ansible, creating the new VMs with ARM (<a href="https://docs.ansible.com/ansible/latest/modules/azure_rm_virtualmachine_module.html">Ansible azure_rm_virtualmachine</a>) and running the installation process.

As for monitoring, there doesn't seem to be a lot of options on the market. The one that I found to be more tailored is Datadog.
Datadog does not only monitor Spark (<a href="https://docs.datadoghq.com/integrations/spark/">Datadog Spark Monitoring</a>) but also Mesos (<a href="https://www.datadoghq.com/blog/monitor-mesos-cluster-datadog/">Datadog Mesos Monitoring</a>).

Last, but not least, is of the utmost importance to create the VMs correctly, not only to Spark or Mesos, but for every system running at Azure.

Microsoft does not provide an availability SLA to a VM by itself, but to a pair of VMs. Furthermore, the VMs for a certain system, need to be created in the same Availability Set, and each VM in different Fault and Update domains (see <a href="https://docs.microsoft.com/en-us/azure/virtual-machines/windows/tutorial-availability-sets">Create and deploy highly available virtual machines with Azure PowerShell</a>).

This assures that each VM is deployed in independent resources (networking, virtualization host, storage, etc.), mitigating failures or that updates occur at the same time in a group of VMs.

All clustered systems based on quorum technology should be deployed with a factor of 2N+1, being N the number of failures that need to be tolerated.





